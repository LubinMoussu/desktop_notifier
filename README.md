<h1 align="center">Personal desktop notifier on latest Covid-19 publications></h1>
<p align="center">Scrap latest french publications related to Covid-19 and referenced in Pubmed and notify latest publications to a personal desktop notifier</p>

## Links
- [API](<https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-covid-publications-france/api/> "Publications françaises #Covid19 référencées dans Pubmed ")
