import json

import requests

from desktop_notifier.parser import Parser

TIMEOUT = 30


class Scraper:
    api_hostname = "https://data.enseignementsup-recherche.gouv.fr"
    api_searchpath = "api/records/1.0/search/"

    header_param = {'Accept': 'application/json'}
    nber_to_scrap = 10000
    dataset = "fr-esr-covid-publications-france"

    def __init__(self, parser=Parser()):
        self.data = None
        self.parser = parser

    def get_response(self, query: dict):
        """

        :param query:
        :return:
        """
        if not isinstance(query, dict):
            raise TypeError

        try:
            response = requests.get(f'{self.api_hostname}/{self.api_searchpath}?', params=query,
                                    headers=self.header_param)
            response.raise_for_status()
            return response
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            print("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            print("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            print("OOps: Something Else", err)

    def fetch_french_publication_data(self) -> dict:
        """

        :return:
        """
        fields = "publication_date,publication_date_week_end,publication_types,journal_name,journal_issns,language,countries,affiliations,authors,keywords,mesh,grant_agency,source"
        query = {"dataset": self.dataset, "q": '', 'facet': {fields}, "rows": self.nber_to_scrap,
                 "sort": "publication_date"}
        response = self.get_response(query=query)
        return json.loads(response.content)

    def parse_data_to_publication(self, data: dict):
        """

        :param data:
        :return:
        """
        if not isinstance(data, dict):
            raise TypeError

        nhits = data["nhits"]
        records = data["records"]
        return self.parser.get_publication_obj_from_records(records)

    @staticmethod
    def get_most_recently_published(publication_list: list) -> list:
        """

        :param publication_list:
        :return:
        """
        if not isinstance(publication_list, list):
            raise TypeError
        lasted_publication_date = publication_list[0].publication_date
        return [elt for elt in publication_list if elt.publication_date == lasted_publication_date]


if __name__ == '__main__':
    scraper = Scraper()
