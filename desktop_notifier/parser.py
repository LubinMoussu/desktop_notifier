from desktop_notifier.publication import Publication


class Parser:
    def __init__(self):
        pass

    @staticmethod
    def instantiate_publications(info: dict):
        """

        :param info:
        :return:
        """
        if not isinstance(info, dict):
            raise TypeError
        if not info:
            raise ValueError("Empty dict")
        return Publication(**info)

    def get_publication_obj_from_records(self, records: list):
        """

        :param records:
        :return:
        """
        if not isinstance(records, list):
            raise TypeError
        return [self.instantiate_publications(elt["fields"]) for elt in records]
