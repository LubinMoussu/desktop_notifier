import datetime
import time

from plyer import notification

from desktop_notifier.scraper import Scraper


class Notifier:
    app_icon = "Medicalwp-Medical-Health-Sign-blue.ico"
    app_name = "COVID19 publications"

    timeout = 30
    sleep_timeout = 30

    def __init__(self, scraper=Scraper()):
        self.scraper = scraper

    def scrap_most_recently_published(self) -> list:
        data = self.scraper.fetch_french_publication_data()
        publications = self.scraper.parse_data_to_publication(data=data)
        return self.scraper.get_most_recently_published(publications)

    @staticmethod
    def get_publication_title(publication_list: list):
        """

        :param publication_list:
        :return:
        """
        if not isinstance(publication_list, list):
            raise TypeError
        return [elt.title for elt in publication_list]

    @staticmethod
    def get_publication_pmid(publication_list: list):
        """

        :param publication_list:
        :return:
        """
        if not isinstance(publication_list, list):
            raise TypeError
        return [elt.pmid for elt in publication_list]

    def create_notification(self, publication_list: list):
        """

        :param publication_list:
        :return:
        """
        if not isinstance(publication_list, list):
            raise TypeError

        publication_date = [elt.publication_date for elt in most_recently_published][0]
        publication_title_list = self.get_publication_title(publication_list)

        for publication_title in publication_title_list:
            notification.notify(
                app_name=self.app_name,
                title=f"COVID19 publications stats on {datetime.date.today()}",
                message=f"{publication_title} publication related to Covid19 and referenced in Pubmed on {publication_date}",
                app_icon=self.app_icon,
                timeout=self.timeout
            )
            time.sleep(self.sleep_timeout)


if __name__ == '__main__':
    notifier = Notifier()
    most_recently_published = notifier.scrap_most_recently_published()
    notifier.create_notification(most_recently_published)

    # TODO notify with link to publication